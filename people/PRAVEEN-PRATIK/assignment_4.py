# import time
from os import system


def visual(road):
    # time.sleep(.5)
    # input()
    # system("clear")
    print("\n")
    sim = "\n".join(
        [" ".join(["--" if pos is None else pos for pos in lane]) for lane in road]
    )
    print(sim)


def assignment_4(look) -> bool:
    while True:
        right = look("right")
        c = 1
        for idx, lane in enumerate(right):
            x = list(set(lane[: idx + 2]))
            if len(x) == 1 and x[0] == None:
                c *= 1
            else:
                c *= 0
        if c == 1:
            visual(list(reversed(right)))
            print("crossed")
            break
    return True
